<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 06/01/2017
 * Time: 14:24
 */

namespace Front\AppBundle\Services;


use Doctrine\ORM\EntityManager;
use Front\AppBundle\Entity\Application;
use Front\AppBundle\Entity\Profile;
use Front\UserBundle\Entity\User;

class ApplicationGetter
{
    private $entityManager;

    /**
     * ApplicationGetter constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $user
     * @return array
     */
    public function getAllApplication(User $user) {
        $externalApplications = $this->getExternalApplication($user);
        $internalApplications = $this->getInternalApplication($user);

        $applications = array_merge($internalApplications, $externalApplications);

        //sort alphabetically
        uasort($applications, function(Application $a,Application $b) {
            return strcmp($a->getName(), $b->getName());
        });

        return $applications;
    }

    public function getCryptedKey() {
        $key = "me";

        $length = 5;
        $today = date("dym");
        return substr(hash('md5', $key . $today), 0, $length); // Hash it
    }

    /**
     * Get the application from Isidore and transform them into application we can use
     * @param User $user
     * @return array
     */
    private function getExternalApplication(User $user) {
        $jsonContent = (file_get_contents("http://vanina/external_application.php?login=".$user->getUsername()."&password=".$this->getCryptedKey()));
        //This is used to convert into UTF8
        $jsonContent = mb_convert_encoding($jsonContent, 'UTF-8',
            mb_detect_encoding($jsonContent, 'UTF-8, ISO-8859-1', true));

        /* For each external apps, I get the unique Id to do the DB request */
        $codes = array();
        foreach ((array) json_decode($jsonContent) as $externalApp) {
            $codes[] = $externalApp->uniqueId;
        }

        return $this->entityManager->getRepository("FrontAppBundle:ApplicationExternal")->getUserApplication($codes);
    }

    /**
     * Get the applications from the app
     * @param User $user
     * @return array
     */
    public function getInternalApplication(User $user) {
        $internalApplications = array();

        foreach ($user->getProfilesApplication() as $profileApplication) {
            /** @var Profile $profileApplication */
            $internalApplications[] = $profileApplication->getApplication();
        }
         return array_unique($internalApplications);
    }

    public function getApplicationNotAccessible(array $applicationAccessible) {
        $applications = $this->entityManager->getRepository("FrontAppBundle:Application")->findAll();

        return array_diff($applications, $applicationAccessible);
    }
}
